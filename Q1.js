var Tempc = 0 //sets Tempc to 0 and defines it as a number
var conversion = 0 //sets coversion to 0 and defines it as a number

tempc = prompt("Please input a temperature in Celcius to be converted") //asks the user to enter a number for the variable tempc

    conversion = tempc * 18 / 10 + 32 //takes the number entered into tempc and multiplies it by 18, divides by 10 and adds 32

    console.log("This application will convert degrees Celcius to degrees Fahrenheit")
    console.log("-------------------------------------------------------------------")
    console.log(`${tempc} degrees Celcius is equal to ${conversion} degrees Fahrenheit`) //outputs the original number entered and displays the calculated number