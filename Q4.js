var letter = "" //defines letter as a blank string
console.log("This application determines whether a letter is a vowel or consonant")
console.log("--------------------------------------------------------------------")
letter = prompt("Enter a letter of the alphabet to determine if it is a vowel or a consonant") //asks the user to enter a letter

    switch(letter) //sets up the switch to check the cases for the input letter
        {
            case "a": //this is the case for the vowel a (lowercase).  If this case is true then the output below will be displayed
            console.log(`a is a vowel`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "e": //this is the case for the vowel e (lowercase). If this case is true then the output below will be displayed
            console.log(`e is a vowel`)
            break //this end the switch so it does not move on to any other cases if this case was true

            case "i": //this is the case for the vowel i (lowercase). If this case is true then the output below will be displayed
            console.log(`i is vowel`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "o": //this is the case for the vowel o (lowercase). If this case is true then the output below will be displayed
            console.log(`o is a vowel`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "u": //this is the case for the vowel u (lowercase). If this case is true then the output below will be displayed
            console.log(`u is vowel`) 
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "y": //this is the case for the vowel y (lowercase). If this case is true then the output below will be displayed
            console.log(`y is sometimes a vowel depending on the circumstances`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "A": //this is the case for the vowel U (uppercase). If this case is true then the output below will be displayed
            console.log(`A is a vowel`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "E": //this is the case for the vowel E (uppercase). If this case is true then the output below will be displayed
            console.log(`E is a vowel`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "I": //this is the case for the vowel I (uppercase). If this case is true then the output below will be displayed
            console.log(`I is vowel`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "O": //this is the case for the vowel O (uppercase). If this case is true then the output below will be displayed
            console.log(`O is a vowel`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "U": //this is the case for the vowel U (uppercase). If this case is true then the output below will be displayed
            console.log(`U is vowel`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            case "Y": //this is the case for the vowel Y (uppercase). If this case is true then the output below will be displayed
            console.log(`Y is sometimes a vowel depending on the circumstances`)
            break //this ends the switch so it does not move on to any other cases if this case was true

            default: //any other letters (upper or lower case) entered will be by default consonants so this will display the results below for anything other than a vowel
            console.log(`${letter} is a consonant`)
            break //this ends the switch

        }