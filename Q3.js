var name = "" //sets variable name to a blank string
var id = "" //sets variable id to a blank string
units = 0 //sets units variable to a number 0
charge = 0 //sets variable charge to a number 0

id = prompt(`Please enter the customers ID number`) //asks the user to enter their id number
name = prompt(`Please enter the customers name`) //asks the user to enter their name
units = prompt(`Please enter the number of units that ${name} has used`) //ask the user to enter the number of units used and used their name to personalise the question

    if(units < 200) //checks the condition of units to see if it is less then 200
        {
            charge = units * 1.2 //if the units is less than 200 then a charge of 1.2 is applied
            console.log("This application will calculate the customer's Electricty Bill:")
            console.log("---------------------------------------------------------------")
            console.log(`ID: ${id}`) //customer id is shown
            console.log(`Name: ${name}`) //customer name is shown
            console.log(`Units: ${units}`) //number of units is shown
            console.log("")
            console.log("Electricty Bill")
            console.log("---------------------------------------------")
            console.log(`Total owing @ 1.2 per unit is: $${charge.toFixed(2)}`) //total charge is shown to two decimal places
        }
    else if(units >= 200 && units < 400) //if the units is between and including 200 and 399 then a charge of 1.5 is applied 
        {
            charge = units * 1.5
            console.log("This application will calculate the customer's Electricty Bill:")
            console.log("---------------------------------------------------------------")
            console.log(`ID: ${id}`)
            console.log(`Name: ${name}`)
            console.log(`Units: ${units}`)
            console.log("")
            console.log("Electricty Bill")
            console.log("---------------------------------------------")
            console.log(`Total owing @ 1.5 per unit is: $${charge.toFixed(2)}`)
        }
    else if(units >= 400 && units < 600) //if the units is between and including 400 and 599 then a charge of 1.8 is applies
        {
            charge  =  units * 1.8
            console.log("This application will calculate the customer's Electricty Bill:")
            console.log("---------------------------------------------------------------")
            console.log(`ID: ${id}`)
            console.log(`Name: ${name}`)
            console.log(`Units: ${units}`)
            console.log("")
            console.log("Electricty Bill")
            console.log("---------------------------------------------")
            console.log(`Total owing @ 1.8 per unit is: $${charge.toFixed(2)}`)
        }
    else //anything 600 or greater in the units variable is charged at a rate of 2.0
        {
            charge = units * 2.0
            console.log("This application will calculate the customer's Electricty Bill:")
            console.log("---------------------------------------------------------------")
            console.log(`ID: ${id}`)
            console.log(`Name: ${name}`)
            console.log(`Units: ${units}`)
            console.log("")
            console.log("Electricty Bill")
            console.log("---------------------------------------------")
            console.log(`Total owing @ 2.0 per unit is: $${charge.toFixed(2)}`)
        }
    
